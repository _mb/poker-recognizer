package com.unknown;

import java.awt.*;

public enum PointColor {
    WHITE,
    RED,
    BLACK,
    UNDEFINED;

    public static PointColor fromRGB(int rgb) {
        Color color = new Color(rgb);
        if (color.getRed() > 200 && color.getGreen() > 200 && color.getBlue() > 200) {
            return PointColor.WHITE;
        } else if (color.getRed() > 180 && color.getGreen() < 100 && color.getBlue() < 100) {
            return PointColor.RED;
        } else if (color.getRed() < 50 && color.getGreen() < 50 && color.getBlue() < 50) {
            return PointColor.BLACK;
        }
        return PointColor.UNDEFINED;
    }
}
