package com.unknown;

import lombok.val;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static java.util.stream.Collectors.toMap;

public class Main {

    private static final String NAME_OF_EMPTY_SAMPLE = "empty.imgsampl";

    public static void main(String[] args) throws IOException {
        File sourceFolder = new File(args[0]);
        String userDir = System.getProperty("user.dir");
        File valuesSamplesFolder = new File( userDir +"\\samples\\values");
        File suitsSamplesFolder = new File(userDir + "\\samples\\suits");
        int width = 35;
        int height = 30;
        System.out.println(valuesSamplesFolder.getAbsolutePath());
        Map<String, ImageCoordinates> valueSamples = Arrays.stream(valuesSamplesFolder.listFiles()).collect(toMap(File::getName, file -> ImageCoordinates.from(file, height, width)));
        Map<String, ImageCoordinates> suitSamples = Arrays.stream(suitsSamplesFolder.listFiles()).collect(toMap(File::getName, file -> ImageCoordinates.from(file, height, width)));
        for (File img : sourceFolder.listFiles()) {
            BufferedImage cardsArea = ImageIO.read(img).getSubimage(143, 585, 350, 90);
            StringBuilder outputBuilder = new StringBuilder(img.getName()).append(" - ");
            for (int startOfCard = 0; startOfCard < cardsArea.getWidth(); startOfCard += 72) {
                BufferedImage cardImage = cardsArea.getSubimage(startOfCard, 0, 62, cardsArea.getHeight());

                BufferedImage valueImage = cardImage.getSubimage(0, 5, width, height);
                ImageSimilarity bestValueMatch = getBestMatch(valueSamples, valueImage);
                outputBuilder.append((bestValueMatch.getSampleName().equals(NAME_OF_EMPTY_SAMPLE) ? "X" : bestValueMatch.getSampleName()));

                BufferedImage typeImage = cardImage.getSubimage(25, 50, width, height);
                ImageSimilarity bestSuiteMatch = getBestMatch(suitSamples, typeImage);
                outputBuilder.append((bestSuiteMatch.getSampleName().equals(NAME_OF_EMPTY_SAMPLE) ? "x" : bestSuiteMatch.getSampleName().substring(0, 1)));
            }
            System.out.println(outputBuilder.toString().replace(".imgsampl", "").replace("Xx", ""));
        }
    }

    private static ImageSimilarity getBestMatch(Map<String, ImageCoordinates> samples, BufferedImage image) {
        ImageCoordinates coordinates = ImageCoordinates.from(image);
        TreeSet<ImageSimilarity> similarityBySample = new TreeSet<>((degree, other) -> Float.compare(degree.getMatch(), other.getMatch()));
        for (val sample : samples.entrySet()) {
            ImageSimilarity similarity = new ImageSimilarity(sample.getKey(), calculateDegreeOfSimilarity(coordinates, sample.getValue()));
            similarityBySample.add(similarity);
        }
        ImageSimilarity maxCandidate = similarityBySample.pollLast();
        return maxCandidate.getSampleName().equals(NAME_OF_EMPTY_SAMPLE) ?
                similarityBySample.last().getMatch() > 0.90 ? similarityBySample.last() : maxCandidate :
                maxCandidate;
    }

    private static float calculateDegreeOfSimilarity(ImageCoordinates imageCoordinates, ImageCoordinates sample) {
        int commonColored = imageCoordinates.numberOfCommonColoredCoordinates(sample);
        int commonUncolored = imageCoordinates.numberOfCommonUnColoredCoordinates(sample);
        return ((float) (commonColored + commonUncolored)) / sample.getHeight() / sample.getWidth();
    }
}
