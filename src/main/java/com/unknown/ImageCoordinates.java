package com.unknown;

import lombok.Getter;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.unknown.PointColor.BLACK;
import static com.unknown.PointColor.RED;
import static java.lang.String.format;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;


public class ImageCoordinates {

    private Set<Coordinate> content;
    @Getter private int height;
    @Getter private int width;

    public ImageCoordinates(int height, int width) {
        this.height = height;
        this.width = width;
        content = new LinkedHashSet<>();
    }

    public static ImageCoordinates from(BufferedImage image) {
        ImageCoordinates imageCoordinates = new ImageCoordinates(image.getHeight(), image.getWidth());
        IntStream.range(0, image.getHeight()).forEach(y -> IntStream.range(0, image.getWidth())
                .filter(x -> isPointColored(image.getRGB(x, y)))
                .forEach(x -> imageCoordinates.add(new Coordinate(x, y))));
        return imageCoordinates;
    }

    public static ImageCoordinates from(File file, int height, int width) {
        ImageCoordinates imageCoordinates = new ImageCoordinates(height, width);
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while (reader.ready()) {
                int y = Integer.parseInt(reader.readLine());
                Arrays.stream(reader.readLine().split(" ")).map(Integer::parseInt).map(x -> new Coordinate(x, y)).forEach(imageCoordinates::add);
            }
        } catch (IOException e) {
            throw new RuntimeException(format("Error reading file: %s", file.getAbsolutePath()), e);
        }
        return imageCoordinates;
    }

    public int numberOfCommonColoredCoordinates(ImageCoordinates other) {
        Set<Coordinate> intersection = new HashSet<>(this.content);
        intersection.retainAll(other.content);
        return intersection.size();
    }

    public int numberOfCommonUnColoredCoordinates(ImageCoordinates other) {
        if (this.getHeight() != other.getHeight() || this.getWidth() != other.getWidth())
            throw new IllegalArgumentException(format("Cannot compare images of different sizes (yet). Comparing heights: %d, %d, widths %d, %d",
                    this.getHeight(), other.getHeight(), this.getWidth(), other.getWidth()));

        ImageCoordinates fullyColored = colored(this.height, this.width);
        fullyColored.content.removeAll(this.content);
        fullyColored.content.removeAll(other.content);
        return fullyColored.content.size();
    }

    public void export(File file) {
        StringBuilder stringBuilder = new StringBuilder(width * height);
        Map<Integer, List<Integer>> groupedCoordinates = content.stream().collect(groupingBy(Coordinate::getY, Collectors.mapping(Coordinate::getX, Collectors.toList())));
        groupedCoordinates.entrySet().stream()
                .filter(entry -> !entry.getValue().isEmpty())
                .sorted(Comparator.comparingInt(Map.Entry::getKey))
                .forEach(listOf_X_CoordsFor_Y_Coord -> {
                    stringBuilder.append(listOf_X_CoordsFor_Y_Coord.getKey()).append("\n");
                    stringBuilder.append(listOf_X_CoordsFor_Y_Coord.getValue().stream().map(String::valueOf).collect(joining(" "))).append("\n");
                });
        try (FileWriter reader = new FileWriter(file)) {
            reader.write(stringBuilder.toString());
        } catch (IOException e) {
            throw new RuntimeException(format("Error exporting image to .imgsmpl file: %s", file.getAbsolutePath()), e);
        }
    }

    private static boolean isPointColored(int rgb) {
        PointColor color = PointColor.fromRGB(rgb);
        return color == BLACK || color == RED;
    }

    private static ImageCoordinates colored(int height, int width) {
        ImageCoordinates imageCoordinates = new ImageCoordinates(height, width);
        IntStream.range(0, height).forEach(y -> IntStream.range(0, width).forEach(x -> imageCoordinates.add(new Coordinate(x, y))));
        return imageCoordinates;
    }

    private void add(Coordinate coordinate) {
        content.add(coordinate);
    }

    @Override
    public String toString() {
        return IntStream.range(0, height).mapToObj(y -> IntStream.range(0, width)
                .mapToObj(x -> (!content.contains(new Coordinate(x, y)) ? "._." : format("%d_%d", y, x))).collect(joining(" ", "", "\n"))
        ).collect(joining());
    }
}