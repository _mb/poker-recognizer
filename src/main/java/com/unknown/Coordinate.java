package com.unknown;

import lombok.Data;
import lombok.NonNull;

@Data
public class Coordinate {

    @NonNull private final int x;
    @NonNull private final int y;
}
