package com.unknown;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ImageSimilarity {

    private String sampleName;
    private float match;
}
